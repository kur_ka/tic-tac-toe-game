import random
import copy

cells_clear = {"A1":"A1", "B1":"B1", "C1":"C1",
               "A2":"A2", "B2":"B2", "C2":"C2",
               "A3":"A3", "B3":"B3", "C3":"C3"}

winning_cells = [["A1", "A2", "A3"],["B1", "B2", "B3"], ["C1", "C2", "C3"],
                ["A1", "B1", "C1"], ["A2", "B2", "C2"], ["A3", "B3", "C3"],
                ["A1", "B2", "C3"], ["A3", "B2", "C1"]]

def grid():
    print()
    print(f" {cells['A1']}| {cells['B1']}| {cells['C1']}\n-----------")
    print(f" {cells['A2']}| {cells['B2']}| {cells['C2']}\n-----------")
    print(f" {cells['A3']}| {cells['B3']}| {cells['C3']}")
    print()

def get_mark():
    while True:
        mark = input("PLAYER 1: Type 'X' if you would like to play with crosses or 'O' for circle: ").upper()
        if mark in ["O", "X"]:
            mark = mark + " "
            return mark
        print("Invalid input")

def game_type():
    while True:
        user_input = input("Would you like to play with computer? Type 'YES' or 'NO': ").upper()
        if user_input in ["YES", "NO"]:
            return user_input
        print("Invalid input")

def player_choice(player_no, mark):
    if play_with_comp == "NO" or player_no == 1:
        while True:
            player_choice = input(f"PLAYER {player_no} ---> Type the cell: ").upper()
            if player_choice in cells:
                return player_choice
            else:
                print("Incorect cell. Type again")
    else:
        return random.choice(list(cells))

def check_if_taken(player_choice):    
    if cells[player_choice] in ["O ", "X "]:
        return True
    else:
        return False

def player_turn(player_no, mark):
    while True:
        player_choice_1 = player_choice(player_no, mark)
        check = check_if_taken(player_choice_1)
        if check == False:
            cells[player_choice_1] = mark
            break
        elif play_with_comp == "NO":
            print("This cell is taken.")

def check_winning(player_no, mark):
    cells_list = list(cells.items())
    mark_cells = [cell[0] for cell in cells_list if cell[1] == mark]

    for lists in winning_cells:
        points = 0 
        for item_1 in lists:     
            if item_1 in mark_cells:
                points = points + 1
            if points == 3:
                return True
                break
    return False

print("Welcome in Tic-Tac-Toe game! \n")
print("Tic Tac Toe is a simple two-player game played on a 3x3 grid.")
print("One player uses 'X' and the other uses 'O'.")
print("Players take turns marking a cell in the grid with their mark.")
print("The objective is to be the first to get three of your symbols in a row, either horizontally,")
print("vertically, or diagonally. If all nine cells are filled without any player achieving three in a row,")
print("the game is a draw. Cells in grid are described as below.")

q = "YES"
while q == "YES":
    cells = copy.deepcopy(cells_clear)    
    grid()
    mark_1 = get_mark()
    mark_2 = "X " if mark_1 == "O " else "O "
    play_with_comp = game_type()
    
    print("Let's play Tic-Tac-Toe! \n \n")
    
    for n in range(1,10):
        if n % 2 != 0:
            player_turn(1, mark_1)
            grid()
            if check_winning(1, mark_1):
                print("Congratulations! You are the winner!")
                break
        else:
            player_turn(2, mark_2)
            if play_with_comp == "YES":
                print("COMPUTER:")
            grid()
            if check_winning(2, mark_2) and play_with_comp == "NO":
                print("Congratulations! You are the winner!")
                break
            elif check_winning(2, mark_2) and play_with_comp == "YES":
                print("UPS, this time computer was better!")
                break
    if not check_winning(1, mark_1) and not check_winning(2, mark_2):
        print("The game is a drew.")
        
    q = input("Would you like to play again? If yes type 'YES'.").upper()
print("Goodbye!")
        
    
 


